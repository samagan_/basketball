﻿using Basketball.Models;
using System;

namespace Basketball
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())

            Seed(db);
            var team = db.Team
                    .Include(p => p.Player)
                    .ThenInclude(c => c.Coach);
        }

        public static void Seed(ApplicationDbContext db)
        {
            if (!db.Team.Any())
            {
                var team = new Team()
                {
                    Name = "SamaganTeam",
                    YearOfCreation = 1899,
                    NumberoFWinsInaSeason = 19,
                    NumberoFLossesInaSeason = 2
                };
                db.Team.Add(team);
                db.SaveChanges();
            }
        }
    }
}
