﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basketball.Enums
{
    public enum Conference
    {
        Western = 1,
        Eastern = 2
    }
}
