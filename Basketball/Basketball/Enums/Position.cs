﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basketball.Enums
{
    public enum Position
    {
        PG = 1,
        SG = 2,
        SF = 3,
        PF = 4,
        C = 5
    }
}
