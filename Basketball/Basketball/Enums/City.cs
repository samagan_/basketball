﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basketball.Enums
{
    public enum City
    {
        LosAngeles = 1,
        California = 2,
        London = 3,
        Berlin = 4,
        Madrid = 5
    }
}
