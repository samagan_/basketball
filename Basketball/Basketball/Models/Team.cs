﻿using Basketball.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basketball.Models
{
    public class Team : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [ForeignKey("Coach")]
        public int CoachId { get; set; }

        [ForeignKey("PlayerID")]
        public int PlayerId { get; set; }

        public Conference Conference { get; set; }

        [Required]
        public int YearOfCreation { get; set; }
        public int NumberoFWinsInaSeason { get; set; }
        public int NumberoFLossesInaSeason { get; set; }
        public virtual Coach Coach { get; set; }
        public virtual ICollection<Player> Players { get; set; }
    }
}
