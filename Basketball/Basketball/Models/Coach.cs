﻿namespace Basketball.Models
{
    public class Coach : Person
    {
        public int NumberOfYearsAsaTrainer { get; set; }
    }
}