﻿using Basketball.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basketball.Models
{
    public class Player : Person
    {
        [Required]
        public double Growth { get; set; }

        [Required]
        public double Weigth { get; set; }
        public int JerseyNumber { get; set; }
        public Position Position { get; set; }
    }
}