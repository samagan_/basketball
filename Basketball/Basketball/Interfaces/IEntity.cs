﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basketball
{
    public interface IEntity<T>
    {
        public int Id { get; set; }
    }
}
