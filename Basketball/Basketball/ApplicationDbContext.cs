﻿using Basketball.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basketball
{
    class ApplicationDbContext : DbContext
    {
        public object Team { get; internal set; }
        DbSet <Team> Teams { get; set; }
        DbSet <Player> Players { get; set; }
        DbSet <Coach> Coachs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Projects\GitRepositories\basketball\Basketball\Basketball");
            builder.AddJsonFile("AppSettings.json");
            var config = builder.Build();

            var connectionString = config.GetConnectionString("DefaultConnection");

            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder
                .Entity<Player>()
                .HasIndex(x => x.JerseyNumber)
                .IsUnique();
            modelBuilder
                .Entity<Team>()
                .HasIndex(x => x.Name)
                .IsUnique();
            modelBuilder
                .Entity<Player>()
                .HasIndex(x => x.TeamId)
                .IsUnique();
        }

    }

}
